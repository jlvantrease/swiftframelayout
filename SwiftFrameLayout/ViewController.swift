//
//  ViewController.swift
//  SwiftFrameLayout
//
//  Created by Jason Vantrease on 1/30/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let teamOne = UIView()
    let teamTwo = UIView()
    let bottomView = UIView()
    var constraintsArray = [NSLayoutConstraint]()
    var stepper1 = UIStepper()
    var stepper2 = UIStepper()
    var team1Lbl = UILabel()
    var team2Lbl = UILabel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        teamOne.translatesAutoresizingMaskIntoConstraints = false
        teamOne.backgroundColor = UIColor.red
        
        self.view.addSubview(teamOne)
        
        let leadingOne = teamOne.leadingAnchor.constraint(equalTo: self.view.leadingAnchor)
        let trailingOne = teamOne.trailingAnchor.constraint(equalTo: self.view.centerXAnchor)
        let topOne = teamOne.topAnchor.constraint(equalTo: self.view.topAnchor)
        let bottomOne = teamOne.bottomAnchor.constraint(equalTo: bottomView.topAnchor)
  

        //team two
        teamTwo.translatesAutoresizingMaskIntoConstraints = false
        teamTwo.backgroundColor = UIColor.blue
        
        self.view.addSubview(teamTwo)
        
        let leadingTwo = teamTwo.leadingAnchor.constraint(equalTo: self.view.centerXAnchor)
        let trailingTwo = teamTwo.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        let topTwo = teamTwo.topAnchor.constraint(equalTo: self.view.topAnchor)
        let bottomTwo = teamTwo.bottomAnchor.constraint(equalTo: bottomView.topAnchor )
        
        
        // bottom View
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        bottomView.backgroundColor = UIColor.purple
        
        self.view.addSubview(bottomView)
        
        let leadingBottom = bottomView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor)
        let trailingBottom = bottomView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        let topBottom = bottomView.topAnchor.constraint(equalTo: teamOne.bottomAnchor)
        let bottomBottom = bottomView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        let bottomsHeight = NSLayoutConstraint(item: bottomView, attribute: .height , relatedBy: .equal, toItem: self.view, attribute: .height, multiplier: 0.20, constant: 0)
        
        
        constraintsArray += [leadingOne, trailingOne, topOne, bottomOne, leadingTwo, trailingTwo, topTwo, bottomTwo, leadingBottom, trailingBottom, topBottom, bottomBottom, bottomsHeight]
        
        NSLayoutConstraint.activate(constraintsArray)
        
        // End of UI views
        
        let team1TxtFld = UITextField(frame: CGRect(x: 0, y: 100.0, width: teamOne.frame.width, height: 45.0))
        
        team1TxtFld.placeholder = "Team One's name"
        team1TxtFld.autoresizingMask = UIViewAutoresizing.flexibleWidth
        team1TxtFld.textAlignment = NSTextAlignment.center
        team1TxtFld.backgroundColor = UIColor.white
        team1TxtFld.layer.borderWidth = 2.0
        team1TxtFld.layer.borderColor = UIColor.black.cgColor
        
        teamOne.addSubview(team1TxtFld)
        
        let team2TxtFld = UITextField(frame: CGRect(x: 1, y: 100.0, width: teamTwo.frame.size.width, height: 45.0))
        
        team2TxtFld.placeholder = "Team Two's name"
        team2TxtFld.autoresizingMask = UIViewAutoresizing.flexibleWidth
        team2TxtFld.textAlignment = NSTextAlignment.center
        team2TxtFld.backgroundColor = UIColor.white
        
        teamTwo.addSubview(team2TxtFld)
        
        team1Lbl = UILabel(frame: CGRect(x: 0, y: 200, width: teamOne.frame.size.width , height: 65))
        team1Lbl.text = "0"
        team1Lbl.textAlignment = NSTextAlignment.center
        team1Lbl.font = UIFont.systemFont(ofSize: 65)
        team1Lbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        team1Lbl.backgroundColor = UIColor.red
        
        teamOne.addSubview(team1Lbl)
        
        team2Lbl = UILabel(frame: CGRect(x: 0, y: 200, width: teamTwo.frame.size.width, height: 65))
        team2Lbl.text = "0"
        team2Lbl.textAlignment = NSTextAlignment.center
        team2Lbl.font = UIFont.systemFont(ofSize: 65)
        team2Lbl.autoresizingMask = UIViewAutoresizing.flexibleWidth
        team2Lbl.backgroundColor = UIColor.blue
        
        teamTwo.addSubview(team2Lbl)
        
        stepper1 = UIStepper (frame: CGRect(x: 10, y: 300, width: teamOne.frame.size.width - 20, height: 60))
        stepper1.minimumValue = 0
        stepper1.maximumValue = 21
        stepper1.value = 0
        teamOne.addSubview(stepper1)
        stepper1.addTarget(self, action: #selector(stepper1Touched(stepper:)), for: UIControlEvents.valueChanged)
        
        
        stepper2 = UIStepper (frame: CGRect(x: 10, y: 300, width: teamOne.frame.size.width - 20, height: 60))
        stepper2.minimumValue = 0
        stepper2.maximumValue = 21
        stepper2.value = 0
        stepper2.backgroundColor = UIColor.black
        teamTwo.addSubview(stepper2)
        stepper2.addTarget(self, action: #selector(stepper2Touched(stepper:)), for: UIControlEvents.valueChanged)
        
        let resetButton = UIButton(type: UIButtonType.system)
        resetButton.frame = CGRect(x: 10, y: 10, width: 80, height: 35)
        resetButton.setTitle("Reset", for: UIControlState.normal)
        resetButton.backgroundColor = UIColor.gray
        //translatesAutoresizingMaskIntoConstraints = false
        bottomView.addSubview(resetButton)
        resetButton.addTarget(self, action: #selector(resetScore(btn:)), for: UIControlEvents.touchDown)
        }
    
    func stepper1Touched(stepper:UIStepper) -> Void {
        team1Lbl.text = String(format: "%.f", stepper1.value)
    }
    
    func stepper2Touched(stepper:UIStepper) -> Void {
        team2Lbl.text = String(format: "%.f", stepper2.value)
    }
    
    func resetScore(btn:UIButton) -> Void{
        team1Lbl.text = "0"
        team2Lbl.text = "0"
        stepper1.value = 0
        stepper2.value = 0
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

